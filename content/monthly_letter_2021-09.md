Title: Monthly Report August 2021
Date: 2021-09-25
Category: Monthly Letter
Authors: Codeberg e.V.

*(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V. this month; as usual published here with due delay. Not member of the non-profit association yet? [Consider joining it!](https://join.codeberg.org))*


Dear Codeberg e.V. Members and Supporters, once again it is time for some updates!

We are hosting 15637 repositories, created and maintained by 13649 users. Compared to one month ago, this is an organic growth rate of +1234 repositories (+8.6% month-over-month) and +872 users (+6.8%).

**Spam users**: By the end of last month, we saw some spam accounts that were obviously created for advertisement or boosting the search engine appearance of websites, some of them being just annoying, some of them also being not-okay in our opinion (scam attempts and potentially offending content). In August, we continued scanning repo content, removing a total of a few hundred user accounts. We received a number of user reports sent to abuse@codeberg.org, reporting problematic content. Thank you for this! Please keep going!  
While we were removing content and user accounts only after applying the many-eyes principle in case of doubt, we should focus on [Codeberg/Community#442](https://codeberg.org/Codeberg/Community/issues/442) for a moderation tool which allows sending users a notice and give them a grace time to appeal. Anyone interested in picking this up?

**Matrix Space**: Early in August, we grouped some Codeberg Matrix Channels into our own Matrix Space, featuring the chat rooms of some well-known Codeberg projects. You can join the space with a Matrix client supporting spaces via [#codeberg-space:matrix.org](https://matrix.to/#/#codeberg-space:matrix.org) and explore and connect with community projects via [#projects-on-codeberg:matrix.org](https://matrix.to/#/#projects-on-codeberg:matrix.org).

**Gitea 1.15**: By the end of the month, we did deploy a new Gitea release, [as announced separately on the blog](https://blog.codeberg.org/introducing-gitea-115.html). This also involved many performance improvements and new features.

**Design updates**: With the Codeberg Documentation following the branding guidelines since the end of July and some works on the navbar together with the Gitea 1.15 release, this is the first time we are touching the Codeberg appearance in a while. Thanks to everyone caring about our branding and improving the user experience.

**Accessibility**: By the end of July, we received some reports that Gitea 1.15 would break accessibility for people who rely on assistive technology like screen readers. Thanks to the Gitea team and external contributors for pushing this matter forward and fixing these issues before the release. This also reminded us to give the already-existent discussion a new push to make sure Codeberg is a working space for everyone.

**Community Discussions**: We have noticed quite some heated discussions among Codeberg members and want to remind everyone to keep conversations productive and non-toxic. We are currently evaluating new moderation strategies, as we feel responsible for keeping Codeberg a welcoming place and will not tolerate offensive behaviour.


The machines are running at about 44% capacity, as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 144 members in total, these are 108 members with active voting rights and 35 supporting members, and one honorary member.

*Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.*

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 

