Title: Monthly Report August 2020
Date: 2020-09-09
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

Once again it is time for some updates.

Official Codeberg.org documentation is now deployed to https://docs.codeberg.org.

A great shout-out to @lhinderberger and @n who have been driving this effort! Contributions are welcome to the source repos https://codeberg.org/codeberg/documentation/ and https://codeberg.org/codeberg-fonts. Please have a look and add your comments and contributions in issues tracker and pull requests!

We are hosting 5021 repositories, created and maintained by 4028 users. Compared to one month ago, this is an organic growth rate of +544 repositories (+12.2% month-over-month) and +446 users (+12.5%).

The machines have been upgraded and are runnnig at about 48% capacity, as usual we will scale up once again we approach the 66% threshold. In the coming weeks we plan to purchase hardware for an economic mid-end backup server doing automated offsite backups (job currently running on machine provided by founding members). If you would like to help in this project -- configuring/building/setting up the box -- please tell us!

Codeberg e.V. has 79 members in total, these are 62 members with active voting rights and 17 supporting members.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username.

Your Codeberg e.V.

--<br/>

https://codeberg.org
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany
Registered at registration court Amtsgericht Charlottenburg VR36929. 

