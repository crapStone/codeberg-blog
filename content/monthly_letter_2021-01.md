Title: Monthly Report December 2020
Date: 2021-01-13
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

We are planning our annual member assembly for February 21st 2021, invitation emails have been sent out last week. If you missed this, please check your inbox for details. If you have proposals for discussion, for votes, or anything else you would like us to put on the agenda, please send an email to codeberg@codeberg.org.

Back to usual business, it is time for monthly updates.

We are hosting 8357 repositories, created and maintained by 6952 users. Compared to one month ago, this is an organic growth rate of +900 repositories (+12.1% month-over-month) and +816 users (+13.3%).

Codeberg e.V. has 97 members in total, these are 74 members with active voting rights and 23 supporting members.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username.

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 


