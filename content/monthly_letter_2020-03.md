Title: Monthly Report February 2020
Date: 2020-03-11
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)



Dear Codeberg e.V. Members and Supporters!

Time for updates.

Voting results for our poll for the new cash auditor are in. Henning received 25 Yes, 2 No, 14 did not vote. Henning is therefore confirmed new auditor, welcome onboard!

We encountered a new wave of spam attacks, spammers trying to submit thousands of issue comments to projects. It took us a few hours to clean everything up, please let us know if you find any left-overs. Also, some improvementes to rate limiting have been implemented.

Codeberg e.V. has 56 members in total, these are 45 members with active voting rights and 11 supporting members. We are hosting 2468 repositories, created and maintained by 1953 users. Compared to one month ago, this is an organic growth of rate of +215 repositories (+9.5% month-over-month) and +174 users (+10%).

As dot-org domains are expected not to remain as cheap as they were, we have extended the codeberg-test.org domain for the maximum 10 years. The main domain codeberg.org had already been extended before.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username.

Your Codeberg e.V.

--

https://codeberg.org
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany
Registered at registration court Amtsgericht Charlottenburg VR36929. 

