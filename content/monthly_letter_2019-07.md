Title: Monthly Report June 2019
Date: 2019-07-28
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month)


Dear Codeberg e.V. Members and Supporters!

It is overdue time for some updates. Sommer doldrums make us lazy. Then again, on the positive side, there were no noteworty incidents.

Codeberg e.V. has 34 active members. We are hosting 729 repositories, created and maintained by 665 users. Compared to one month ago, this is an organic growth of rate of +75 repositories (+11% month-over-month), and +72 users (+12%).

The machines are runnnig at about 29% capacity, we will scale up as soon we approach the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggest that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers.

Then we extended the codeberg.org domain for 9 years in order to lock in current prices, after recent news raised concerns about changing ICANN policy that will make dot.org domains much more expensive.

We have worked out most details of our secure voting system, voting tokens for the upcoming bylaw change poll will be sent out via email next days. Please keep an eye on your inbox.

Codeberg e.V.

